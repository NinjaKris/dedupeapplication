package com.heaven;

import com.heaven.models.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.json.simple.parser.JSONParser;
import org.junit.Test;

import java.util.zip.DataFormatException;

//TODO Implement Me!
public class UserModelTests {

    private JSONParser jsonParser;
    JSONObject objectFromString;

    @Before
    public void setup() {
        jsonParser = new JSONParser();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowErrorGivenNullId() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":null,\"email\":\"test@test.com\"}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User.of(objectFromString);

        //verify
        //Verification is at the top of the file ..(expected = NullPointerException.class)
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowErrorGivenEmptyId() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":\"\",\"email\":\"test@test.com\"}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User.of(objectFromString);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowErrorGivenNullEmail() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":\"8dnsjke8\",\"email\":null}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User.of(objectFromString);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowErrorGivenEmptyEmail() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":\"mklnsd902\",\"email\":\"\"}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User.of(objectFromString);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowErrorGivenNullDate() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":\"8dnsjke8\"," +
                "\"email\":\"test@test.com\"," +
                "\"entryDate\":null}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User.of(objectFromString);
    }

    @Test(expected = DataFormatException.class)
    public void shouldThrowErrorGivenBadDateFormat() throws ParseException, DataFormatException {
        //setup
        String jsonString = "{\"_id\":\"8dnsjke8\"," +
                "\"email\":\"test@test.com\"," +
                "\"entryDate\":\"10/25/2016\"}";
        objectFromString = (JSONObject) jsonParser.parse(jsonString);

        //execute
        User user = (User) User.of(objectFromString);
    }
}
