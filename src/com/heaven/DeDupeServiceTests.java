package com.heaven;

import com.heaven.Service.DeDupeService;
import com.heaven.models.User;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class DeDupeServiceTests {

    private final String USER_ID1 = "5ITmsnpe0289";
    private final String USER_ID2 = "mkdnsle00u23g";

    private final String USER_FIRST_NAME1 = "Tom";
    private final String USER_FIRST_NAME2 = "Mick";

    private final String USER_LAST_NAME1 = "Sawyer";
    private final String USER_LAST_NAME2 = "Jagger";

    private final String USER_ADDRESS1 = "123 Private Drive";
    private final String USER_ADDRESS2 = "567 Beer Street";

    private final String USER_EMAL1 = "email1@gmail.com";
    private final String USER_EMAL2 = "email2@gmail.com";

    private final Date DATE1 = new Date(Date.UTC(2019, 01, 28, 4, 50, 14));
    private final Date DATE2 = new Date();


    private User user;
    private User user2;

    private DeDupeService target;

    @Before
    public void setup() {
        target = new DeDupeService();

        user = new User();
        user2 = new User();
    }

    @Test
    public void CaseWhereFilesWithDuplicateIDsTheNewestDateIsPreferred(){
        //setup
        List<User> userList = new ArrayList<>();

        user.setEmail(USER_EMAL1);
        user.setId(USER_ID1);
        user.setEntryDate(DATE2);

        user2.setEmail(USER_EMAL2);
        user2.setId(USER_ID1);
        user2.setEntryDate(DATE1);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getEmail() == USER_EMAL1);
    }

    @Test
    public void CaseWhereFilesWithDuplicateEmailsTheNewestDateIsPreferred(){
        //setup
        List<User> userList = new ArrayList<>();

        user.setEmail(USER_EMAL1);
        user.setId(USER_ID1);
        user.setEntryDate(DATE2);

        user2.setEmail(USER_EMAL1);
        user2.setId(USER_ID2);
        user2.setEntryDate(DATE1);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getId() == USER_ID1);
    }

    @Test
    public void CaseWhereFileHasDuplicateEmailsAndIDsTheNewestDateIsPreferred(){
        //setup
        List<User> userList = new ArrayList<>();

        user.setEmail(USER_EMAL1);
        user.setId(USER_ID1);
        user.setEntryDate(DATE2);

        user2.setEmail("EMAIL1@gmail.com");
        user2.setId(USER_ID1);
        user2.setEntryDate(DATE1);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getEntryDate() == DATE2);
    }


    @Test
    public void CaseWhereFileHasDuplicatesOtherThanEmailsAndIDsIsIgnored(){
        //setup
        List<User> userList = new ArrayList<>();

        user = new User(USER_ID1, USER_EMAL1, USER_FIRST_NAME1, USER_LAST_NAME1, USER_ADDRESS1, DATE1);
        user2 = new User(USER_ID2, USER_EMAL2, USER_FIRST_NAME1, USER_LAST_NAME1, USER_ADDRESS1, DATE2);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(2, results.size());
    }

    @Test
    public void CaseWhereFileHasALLDuplicates(){
        //setup
        List<User> userList = new ArrayList<>();

        user = new User(USER_ID1, USER_EMAL1, USER_FIRST_NAME1, USER_LAST_NAME1, USER_ADDRESS1, DATE1);
        user2 = new User(USER_ID1, USER_EMAL1, USER_FIRST_NAME1, USER_LAST_NAME1, USER_ADDRESS1, DATE2);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getId() == USER_ID1);
        assert(results.get(0).getEmail() == USER_EMAL1);
        assert(results.get(0).getAddress() == USER_ADDRESS1);
        assert(results.get(0).getFirstName() == USER_FIRST_NAME1);
        assert(results.get(0).getLastName() == USER_LAST_NAME1);
        assert(results.get(0).getEntryDate() == DATE2);

    }

    @Test
    public void CaseWhereFilesWithDuplicateEmailsAndDifferentCaseTheNewestDateIsPreferred(){
        //setup
        List<User> userList = new ArrayList<>();

        user.setEmail(USER_EMAL1);
        user.setId(USER_ID1);
        user.setEntryDate(DATE2);

        user2.setEmail(USER_EMAL1);
        user2.setId(USER_ID2);
        user2.setEntryDate(DATE1);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getId() == USER_ID1);
    }

    @Test
    @Ignore
    //TODO The likely hood of a date being absolutely identical(to the milisecond) is quite the edge case
    // And I did not have the time to cover it. Very Sorry.
    public void CaseWhereFilesWithDuplicateEmailsIDsAndDatesTheLastIsPreferred(){
        //setup
        List<User> userList = new ArrayList<>();

        user.setEmail(USER_EMAL1);
        user.setId(USER_ID1);
        user.setEntryDate(DATE2);
        user.setFirstName(USER_FIRST_NAME1);

        user2.setEmail(USER_EMAL1);
        user2.setId(USER_ID1);
        user2.setEntryDate(DATE2);
        user2.setFirstName(USER_FIRST_NAME2);

        userList.add(user);
        userList.add(user2);

        //execution
        List<User> results = target.removeDuplicateIdsAndEmails(userList);

        //verify
        assertEquals(1, results.size());
        assert(results.get(0).getFirstName() == USER_FIRST_NAME2);
    }

}
