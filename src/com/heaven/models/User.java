package com.heaven.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.simple.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.DataFormatException;

@Data
//The following constructor annotations are for testing. I know it's not proper
// with a static constructor, but It's 12am and I'm tired haha.
@AllArgsConstructor
@NoArgsConstructor
public class User {
    String id;

    String email;

    String firstName;

    String lastName;

    String address;

    Date entryDate;

    public static Object of(JSONObject userToAdd) throws DataFormatException {
        User user = new User();

        user.id = (String) userToAdd.get("_id");
        if(user.id == null || user.id.isEmpty()){
            System.out.println("Users must have a non-null Id");
            throw new NullPointerException();
        }

        user.email = (String) userToAdd.get("email");
        if(user.email == null || user.email.isEmpty()){
            System.out.println("Users must have a non-null Email");
            throw new NullPointerException();
        }

        user.firstName = (String) userToAdd.get("firstName");
        user.lastName = (String) userToAdd.get("lastName");
        user.address = (String) userToAdd.get("address");

        if(userToAdd.get("entryDate") == null){
            System.out.println("Users must have a non-null Date");
            throw new NullPointerException();
        }

        try {
            user.entryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .parse((String) userToAdd
                            .get("entryDate"));
        }catch (ParseException e){
            System.out.println("Dates must be in UTC date time format!");
            throw new DataFormatException();
        }

        return user;
    }
}
