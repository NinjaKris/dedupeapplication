package com.heaven.Service;

import com.heaven.models.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

import static com.heaven.models.User.of;

public class FileService {
    JSONParser jsonParser = new JSONParser();
    List<User> userList = new ArrayList<>();

    public List<User> read(String fileName) throws IOException, ParseException {
        System.out.println("Reading from file: " + fileName);

        FileReader jsonFile = new FileReader(fileName);
        Object obj = jsonParser.parse(jsonFile);

        JSONArray userArray = (JSONArray) ((JSONObject) obj).get("leads");
        if(userArray == null) {
            System.out.println("Bad JSON File, Try Again!");
            throw new NullPointerException();
        }
        userArray.forEach( user -> {
            try {
                convertToUser( (JSONObject) user );
            } catch (DataFormatException e) {
                //GAH!!! Why won't this print Eh?
                // TODO Figure it out later
                System.out.println("Bad User Data, Try Again!");
            }
        });
        System.out.println("Found " + userList.size() + " users");
        return userList;
    }


    private void convertToUser(JSONObject userToAdd) throws DataFormatException {
        System.out.println("Adding User to list: " + userToAdd.toString());
        User user = (User) of(userToAdd);
        userList.add(user);
    }
}
