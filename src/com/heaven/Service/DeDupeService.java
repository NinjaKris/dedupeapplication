package com.heaven.Service;

import com.heaven.models.User;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DeDupeService {
    public List<User> removeDuplicateIdsAndEmails(List<User> users) {

        return users.stream()
                .sorted(Comparator.comparing(User::getEntryDate))
                .filter(distinctByKey(User::getId))
                .filter(distinctByKey(User::getEmail))
                .collect(Collectors.toList());
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
