package com.heaven;



import com.heaven.Service.DeDupeService;
import com.heaven.Service.FileService;
import com.heaven.models.User;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        FileService fileService = new FileService();
        DeDupeService deDupeService = new DeDupeService();
        List<User> users;

        System.out.println("Enter the name of the file that you wish to de-dupe");
        while(true) {
            Scanner input = new Scanner(System.in);
            String fileName = input.nextLine();

            if(fileName.equalsIgnoreCase("quit"))
                break;
            else {
                try {
                    users = fileService.read(fileName);
                }
                catch (FileNotFoundException e){
                    System.out.println("Sorry, I can't find your file. Try again!!");
                    continue;
                }

                List<User> uniqueUserList = deDupeService.removeDuplicateIdsAndEmails(users);

                System.out.println("\nThe following users are unique:");
                uniqueUserList.forEach(user -> System.out.println(user.getId()+", "+
                        user.getEmail()+", "+ user.getEntryDate()));

                System.out.println("\nThe number of proper users without dupes is: " +uniqueUserList.size());

                System.out.println("\n\nEnter another file that you wish to de-dupe" );
            }
        }
    }
}
