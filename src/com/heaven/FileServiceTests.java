package com.heaven;

import com.heaven.Service.FileService;
import com.heaven.models.User;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileServiceTests {

    private FileService target;

    @Before
    public void setup() {
        target = new FileService();
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowFileNotFoundException() throws IOException, ParseException {
        //setup
        String fileName = "../testFiles/test.json";

        //execution
        target.read(fileName);

        //verify
        //Verify is at the top ...(expected = FileNotFoundException.class)
    }

    @Test
    public void shouldReadFileThatExists() throws IOException, ParseException {
        //setup
        String fileName = "../testFiles/leadsTestFile.json";

        //execution
        List<User> userList =  target.read(fileName);

        //verify
        assertEquals(10, userList.size());
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullExceptionWithBadTestFile() throws IOException, ParseException {
        //setup
        String fileName = "../testFiles/leadsBadTestFile.json";

        //execution
        target.read(fileName);

        //verify
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowParseExceptionWithBadUserTestFile() throws IOException, ParseException {
        //setup
        String fileName = "../testFiles/leadsBadUserTestFile.json";

        //execution
        target.read(fileName);

        //verify
    }
}
