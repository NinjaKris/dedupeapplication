### Assumptions: ###
* **JSON files are stored within the root of the file.**
* Filenames must be entered into the command line in able to de-duplicate the
file.
* In order to exit the program must enter the word "quit".
* The file will not be overwritten or modified with deduped data.
* (As per instructed): the program can do everything in memory.
* Case is ignored
* The following test cases, for the sake of time, will be skipped:
    * Whether a program has exited because of user input.
    * Logs
    * Main(runtime)
* Proper email structure is not being validated at this time. 
* File types are not validated at this time.


### Test Cases: ###
- [X] Given a JSON file with duplicate id's, the newest date will be preferred.
- [X] Given a JSON file with duplicate emails, the newest date will be preferred.
- [X] Given a JSON file with duplicate id's and email, the newest date will be preferred.
- [X] Given a JSON file with duplicates in any other attribute will be ignored.
- [ ] Given a JSON file with any qualifying duplicate that has the same date, the 
last will be preferred.
- [X] Given a JSON file with duplicate emails and a different case, the newest date will be preferred.
- [X] Given a JSON file with null id's, a data structure error is thrown.
- [X] Given a JSON file with null emails, a data structure error is thrown.
- [X] Given a JSON file with any qualifying duplicate, the output is verified.
- [X] Given a JSON file with only all qualifying duplicates, the output is verified as only having one record.
- [X] Given a JSON file with now qualifying duplicates, the output is verified as only having all
records returned.
- [X] Given a JSON file not located within the root dir, a FileNotFound Exception is thrown.
- [X] Given a JSON file Leads must meet the name requirements of the json object otherwise a data structure 
error is thrown
- [X] Given a JSON file with a date in a format other than UTC time, a DataError Exception is thrown
- [X] Given a JSON file with a null date an Exception is thrown



#### Notes: ####
- For the sake of time and effort, I will not be writing my own JSON parser.
- I used Gradle and InteliJ in my solution

